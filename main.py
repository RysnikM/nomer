import time
import os
import sys
import matplotlib.image as mpimg

# ----------------------------------------------------------------------------------------------------------------------
# change this property
NOMEROFF_NET_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.getcwd())
MASK_RCNN_DIR = os.path.join(NOMEROFF_NET_DIR, 'model')
MASK_RCNN_LOG_DIR = os.path.join(NOMEROFF_NET_DIR, 'logs')
sys.path.append(NOMEROFF_NET_DIR)
print(MASK_RCNN_DIR, MASK_RCNN_LOG_DIR)
# ----------------------------------------------------------------------------------------------------------------------
# Import license plate recognition tools.
from NomeroffNet import filters, RectDetector, TextDetector, OptionsDetector, Detector, textPostprocessing

def loadModel():
    global nnet, textDetector, rectDetector, optionsDetector
    try:
        # Initialize npdetector with default configuration file.
        nnet = Detector(MASK_RCNN_DIR, MASK_RCNN_LOG_DIR)
        nnet.loadModel("latest")

        rectDetector = RectDetector()

        optionsDetector = OptionsDetector()
        optionsDetector.load("latest")

        # Initialize text detector.
        textDetector = TextDetector.get_static_module("eu")()
        textDetector.load("latest")

        return True
    except:
        return False

def detect(img):
    print("START RECOGNIZING")

    startTime = time.time()
    # img_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'ex2.jpeg')
    # read image in RGB format
    # p.s. opencv read in BGR format
    # img = mpimg.imread(img_path)
    # DETECT
    NP = nnet.detect([img])
    # Generate image mask.
    cv_img_masks = filters.cv_img_mask(NP)

    # Detect points.
    arrPoints = rectDetector.detect(cv_img_masks)
    zones = rectDetector.get_cv_zonesBGR(img, arrPoints)

    # find standart
    regionIds, stateIds, countLines = optionsDetector.predict(zones)
    regionNames = optionsDetector.getRegionLabels(regionIds)

    # find text with postprocessing by standart
    textArr = textDetector.predict(zones)
    textArr = textPostprocessing(textArr, regionNames)

    return textArr
    # print(textArr)
    # print(time.time() - startTime)

if __name__ == '__main__':
    print(loadModel())
    im = mpimg.imread('ex2.jpeg')
    t1 = time.time()
    print(detect(im))
    print(time.time() - t1)
